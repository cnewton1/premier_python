#=========================================================================================
# Use this version to double check that the file already exist'
# To Prevent Download Download
# (RECOMMENDED VERSION TO USE WHEN DOWNLOADING IMAGE)
#=========================================================================================
import csv
import urllib.request
import time

import os.path
from os import path


HOMEDIR="/d/roy/NEWD/"

OUTPATH="/d/roy/ROY_DOWNLOADIMAGES/"
OUTDIR="/d/roy/ROY_DOWNLOADIMAGES/"

INPUTFILE=HOMEDIR+"FILELIST_DOWNLOAD.csv"

# INPUTFILE=HOMEDIR+"TBT_QUICK2.csv"


def getImageNameOnly(text):
    j=text.rfind("/")+1
    endofstring=len(text)
    gg=text[j:endofstring]
    return (gg)


def MakeDirectory(DirectoryName):

    access_rights = 0o777

    try:
        os.mkdir(DirectoryName,access_rights);
   
    except OSError as error:
        print(error)         
  
    return (DirectoryName)


def getStoreNumberOnly(text):
    global OUTDIR;
    jend=text.rfind("/")
    j=text.find("/images/")
    j=j+8
    endofstring=len(text)
    storeNumberString=text[j:jend]

    MDIR=OUTDIR + storeNumberString  
    return MDIR

def getStoreNumX(text):
    global OUTDIR
    jend=text.rfind("/")
    j=text.find("/images/")
    j=j+8
    endofstring=len(text)
    storeNumberString=text[j:jend]
    MDIR=storeNumberString  
    return MDIR


SCOUNT=0
#----------------------------------------------------------------------------------------------------------------------------------------------
# DOWNLOAD IMAGE
#    urllib.request.urlretrieve("https://assetserver.net/_assets/samsung/1000/", "F:\WESTCARB_IMAGES\\20Z874_AS01.JPG")
#----------------------------------------------------------------------------------------------------------------------------------------------
def DownloadImage(ImageName):
    global OLDFILENAME
    URL=ImageName
    URL="https://tbtportal2.com/" + ImageName
    print(URL)
    
    FileNameOnly=getImageNameOnly(URL)
    OUTFILE = OUTDIR + FileNameOnly
    print("Requesting..." + URL)
    try:
        urllib.request.urlretrieve(URL,OUTFILE)
        print ("OUTNAME=",OUTFILE)
    except:
        print ("OUTNAME=",OUTFILE)
        print ("Error Downloading", URL)

    time.sleep(2)
    
    return

def LocateImage(ImageName):
    locationFile=OUTDIR + getImageNameOnly(ImageName)
    j=path.exists(locationFile)
    return j
"""
==============================================
              main
==============================================
"""
count = 0
OLDFILENAME=""
Mainlist=[]
TAB = "\t"
SCOUNT=0
scanCount=1
OLDDIR=""
OLDV=""

with open(INPUTFILE,'r', encoding="raw_unicode_escape") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        if (len(row) < 1):
            continue
        ImageName = row[0]
        if (len(ImageName) < 5):
            continue

        if (OLDFILENAME != ImageName):
            k=LocateImage(ImageName)
            if (k==0):
                if ((ImageName.find("==") > -1) or
                    (ImageName.find("JPEG") > -1) or
                    (ImageName.find("png") > -1) or                    
                    (ImageName.find("jpeg") > -1)):                
                    SKIP=1
                else:
                    """ Bfore we download let check the directory store number"""
                    DIRNAME=getStoreNumberOnly(ImageName)
                    TESTDIR=getStoreNumX(ImageName)
                    if (OLDV != TESTDIR):
                        #new make director when store number changes in the file
                        MAKEDIRNAME=OUTPATH + TESTDIR
                        OUTDIR=MakeDirectory(MAKEDIRNAME)

                        OLDV = TESTDIR
                        OLDDIR = OUTDIR
                    
                        

                    DownloadImage(ImageName)
                    # print(ImageName)             
                    SCOUNT=SCOUNT+1
                    print("NUMBER: ",SCOUNT)
                    SKIP=0
            else:
                print("Already Exist ",ImageName, "count=",scanCount)
                scanCount=scanCount+1

                count = count + 1

            OLDFILENAME=ImageName
            if (SCOUNT > 20000):
               break


print("Number of items process for upload is: ", count)

if (SCOUNT > 800):
    print("Max for this session has been reach re-run: ", SCOUNT)

