# ------------------------------------------------------------------------------
# LASTEST PROGRAM TO UPLOAD ORS Data into the westcarb website.
# 1) We calculate the Case PK
# 2) and add the $5.00 small processing fee if lower than 5
# 3) Added [ SOLD in PACK OF ] in description
# ================================================================================
import csv

outputlist = []
nodup = []
line = ""
bypass = 0
count = 0
countj = 0
processCount = 0
TAB = "\t"
WebPrice = 0

def unique(input):
    outputlist.append(input)


def GetUnique():
    outputlist.sort()
    m = len(outputlist)
    i = 0
    toCt = 0
    Old_lookString = ""
    while (i < m):
        lookString = outputlist[i]
        i = i + 1
        if (Old_lookString != lookString):
            nodup.append(lookString)
            toCt = toCt + 1

        Old_lookString = lookString  # transfer
    return (toCt)


def removeAuto(input):
    Z = 0
    count = 0
    A = input.find("Auto")
    B = input.find("Car")
    C = input.find("auto")
    D = input.find("motor")

    Z = A + B + C + D

    if (Z < 1):
        input = input + "\n"
        outputlist.append(input)
    else:
        count = 1

    return count


def CleanIt(text):
    text = text.replace(',', ';')
    text = text.replace(', ', ";")
    text = text.replace("\t", ";")
    return (text)


def CleanPrice(text):
    text = text.replace(',', '')
    text = text.replace("\t", "")
    text = text.replace("$", "")
    return (text)


def AddStep(ICount):
    I = 0
    mString = ""
    while (I < ICount + 1):
        mString = mString + "\t"
        I = I + 1
    return (mString)


def strip_non_ascii(string):
    ''' Returns the string without non ASCII characters'''
    stripped = (c for c in string if 0 < ord(c) < 127)
    return ''.join(stripped)


def checkRef(imageRef, FOB):
    if (imageRef.find("http:") > -1) and (FOB == "YES"):
        return 1
    return 0


def getIntegerPart(String1):
    mm = int(re.search(r'\d+', String1).group())
    return mm


def getTextPart(String1):
    String2 = String1
    mm = re.search(r'\d+', String2).group()
    mm = String1.replace(mm, "")
    return mm


def getCategory(text):
    j=text.find(">")
    m=text[:j]
    return (m)

def getSubCategory(text):
    j=text.find(">")
    left=text[j:]
    j = left.find(">",1)+1
    k = len(left)
    m = left[j:k]
    return (m)

def AddWCEPRFIX(text):
    path = str(text)
    if (path != ""):
        path = "WCE" + path
        return path
    else:
        return ""

def RemoveQuote(text):
    path = str(text)
    if (path != ""):
        path = path.replace("\"","")
        path = path.replace(',', ';')
        return path
    else:
        return ""

def formatNumbers(text):
    path = str(text)
    if (path != ""):
        jj=float(text)
        jj=(jj *.20) + jj # add 20% to the price
        path = "{:8.2f}".format(jj)
        return path
    else:
        return "0.00"


def SpaceTab(number):
    i=0
    out=""
    while(i < number):
        i=i+1
        out=out+" \t"

    return out


"""----------------------------------------
              main
"""

fout = open('/home/newton/WCE_GLOBAL/GLOBAL_PRODUCT_KAL.txt', 'w')

line = "Item_Num" + TAB + "Category" + TAB + "Subcategory" + TAB + "header" + TAB + "subheader" + TAB + "description" + TAB + "price" + TAB + \
        "comment" + TAB + "filename" + TAB + "B1" + TAB + "B2" + TAB + "B3" + TAB + "B4" + TAB + "B5" + TAB + \
        "B6" + TAB + "B7" + TAB + "Image_GAL" + TAB + "XTABLEX" + TAB + "CS_TABLE" + TAB + "Shipping" + TAB + "discount" + TAB + "case_pk" + \
        TAB + "min_qty" + TAB + "active" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + \
        "-" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + "-" + TAB + "\n"

fout.write(line)
skip=False
wrote=0
with open('/opt/WCE_GLOBAL/GLOBAL_PRODUCT.txt', 'r', encoding="raw_unicode_escape") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter='\t')
    next(csv_reader)
    for row in csv_reader:
        itemRef=row[0]
        itemTitle=row[1]
        Description=row[2]
        Item_Abbreviated_Description=row[3]
        Brand=row[4]
        Manufacturer=row[5]

        PRODNAME = itemTitle
        Mfr_Part_Number=row[6]
        Color=row[7]
        Material=row[8]

        Unit_Of_Measure=row[9]
        Item_Dimension_Unit=row[10]

        Item_Length=row[11]
        Item_Width=row[12]
        Item_Height=row[13]
        Item_Weight=row[14]

        Physical_Dimensions=row[15]
        Item_Package_Qty=row[16]
        Item_Lot_Qty=row[17]

        SKU=Mfr_Part_Number
        Published_Price = row[18]
        Published_Price = row[19]

        Drop_Stock=row[20]
        Country_of_Origin=row[21]
        Item_Height=row[13]
        Item_Weight=row[14]
        KEYWORD=row[47]
        IMAGE_URL=row[51]
        ALT_IMAGE = IMAGE_URL
        P_WWW = IMAGE_URL

        UPCCode = row[57]
        SKU = row[58]

        IMAGE_URL=IMAGE_URL.replace("http://www.westcarb.com/ProductImages/images","http://www.kalsecure.com/ProductImage/images")



        FOB="N"
        HAZMAT="N"
        ISSCODE=" "
        WEIGHT=Item_Weight

        Category=getCategory(KEYWORD)
        SubCategory=getSubCategory(KEYWORD)
        VENDPART = "WCE" + Mfr_Part_Number
        WCE_PART_NUMBER = "WCE" + Mfr_Part_Number
        QTY_UNIT=Item_Package_Qty
        QP_UNIT=Item_Package_Qty

        #--------------------------------------
        PRICE_UNIT=formatNumbers(Published_Price)
        LEAD_TIME="10"
        CUSTOMER=PRICE_UNIT
        count = count+1
        PPOINT=" "
        SIN=" "
        INCR_OF=Unit_Of_Measure

# Tools & Instruments
        skip=True
        if (Category.find("Safety & Security") == 0):
            skip=False
        """
        line = Description + TAB + itemTitle + TAB + WCE_PART_NUMBER + TAB + Description + TAB + SKU + TAB + Mfr_Part_Number + TAB + Manufacturer + \
               TAB + PRICE_UNIT + TAB + "99" + TAB + Unit_Of_Measure + TAB + Item_Package_Qty + TAB + LEAD_TIME + TAB + CUSTOMER + TAB + KEYWORD + TAB +\
               Country_of_Origin + TAB + Category + TAB + SubCategory + TAB + ALT_IMAGE + TAB + IMAGE_URL + TAB + FOB + TAB +  Item_Length + TAB + \
               Item_Height + TAB + Item_Width + TAB + Item_Width + TAB +  Item_Weight + TAB + HAZMAT + TAB + "Y" + TAB + \
               PRODNAME + TAB + PRODNAME + TAB + VENDPART + TAB + itemTitle +  TAB + ISSCODE + TAB +  QTY_UNIT + TAB + QP_UNIT + TAB + WEIGHT + TAB + \
               SIN + TAB + PPOINT + TAB + INCR_OF + TAB + P_WWW + TAB + WCE_PART_NUMBER + TAB + "GLOBAL" + TAB
        """

        line = WCE_PART_NUMBER + TAB + Category + TAB + SubCategory + TAB + itemTitle + TAB + PRODNAME + TAB + Description + TAB + CUSTOMER + TAB + KEYWORD + TAB + IMAGE_URL + TAB + \
               Item_Length + TAB + Item_Height + TAB + Item_Width + TAB + Item_Width + TAB +  Item_Weight + TAB + HAZMAT + TAB + SpaceTab(8) + "Y" + TAB

        line = line + "\n"

        if (skip == False):
            fout.write(line)
            wrote=wrote+1


print("Number of items read: ", count)
print("Number of items process for upload is: ", wrote)
fout.close()
