#=========================================================================================
# Use this version to double check that the file already exist'
# To Prevent Download Download
# (RECOMMENDED VERSION TO USE WHEN DOWNLOADING IMAGE)
#=========================================================================================
import csv
import urllib.request
import time
import os.path
from os import path

HOMEDIR="/d/roy/NEWD/"
OUTDIR="/d/roy/ROY_DOWNLOADIMAGES/"

INPUTFILE=HOMEDIR+"FILELIST_DOWNLOAD.csv"

# INPUTFILE=HOMEDIR+"SPLIT2/Westcarb_IMAGE_PART2_000005B.txt"

def About():
    print("=======================================================================")
    print("\n               CHRONICLES SYSTEMS INC");
    print("     IMAGE DOWNLOAD Ver. 2.0 Copyright(c) 2019-2021\n")   
    print("=======================================================================")

def getImageNameOnly(text):
    j=text.rfind("/")
    endofstring=len(text)
    gg=text[j+1:endofstring]
    return (gg)



def ReadConfig():
    global HOMEDIR
    global OUTDIR
    global INPUTFILE

   

    try:
        with open("./downloadconfig.cfg",'r', encoding="raw_unicode_escape") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                if (len(row) < 1):
                    continue
                AttributeName = row[0]
                AttriValue= row[1]
                print (AttributeName + " " + AttriValue)
                if (AttributeName == "HOME_DIRECTORY"):
                    HOMEDIR = AttriValue

                if (AttributeName == "OUTPUT_DIRECTORY"):
                    OUTDIR = AttriValue

                if (AttributeName == "CSV_INPUT_FILE"):
                    INPUTFILE = HOMEDIR+AttriValue       
            print ("\nReading the CSV FILE : ->", INPUTFILE,"\n")        

    except OSError as err:
        print("\nERROR the configuration file was not found!\nPlease create a downloadconfig.cfg file and retry!\n")                      
        exit()
                





SCOUNT=0
#----------------------------------------------------------------------------------------------------------------------------------------------
# DOWNLOAD IMAGE
#    urllib.request.urlretrieve("https://assetserver.net/_assets/samsung/1000/", "F:\WESTCARB_IMAGES\\20Z874_AS01.JPG")
#----------------------------------------------------------------------------------------------------------------------------------------------
def DownloadImage(ImageName):
    global OLDFILENAME
    global OUTDIR
    ImageName = ImageName.replace(" ","%20")

    URL=ImageName
    FileNameOnly=getImageNameOnly(URL)
    OUTFILE = OUTDIR + FileNameOnly
    print("Requesting..." + URL)
    try:
        urllib.request.urlretrieve(URL,OUTFILE)
    except:
        print ("Error Downloading", URL)

    time.sleep(1)
    return


def getImageNameOnly(text):
    j=text.rfind("/")+1
    endofstring=len(text)
    gg=text[j:endofstring]
    return (gg)


def LocateImage(ImageName):
    locationFile=OUTDIR + getImageNameOnly(ImageName)
    j=path.exists(locationFile)
    return j
"""
==============================================
              main
==============================================
"""
count = 0
OLDFILENAME=""
Mainlist=[]
TAB = "\t"
SCOUNT=0
scanCount=1

About()
ReadConfig()

with open(INPUTFILE,'r', encoding="raw_unicode_escape") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        if (len(row) < 1):
            continue
        ImageName = row[0]
        if (len(ImageName) < 5):
            continue

        if (OLDFILENAME != ImageName):
            k=LocateImage(ImageName)
            if (k==0):
                DownloadImage(ImageName)
                SCOUNT=SCOUNT+1
                print("NUMBER: ",SCOUNT)
            else:
                print("Already Exist ",ImageName, "count=",scanCount)
                scanCount=scanCount+1

                count = count + 1

            OLDFILENAME=ImageName
            if (SCOUNT > 1000):
                break


print("Number of items process for upload is: ", count)

if (SCOUNT > 800):
    print("Max for this session has been reach re-run: ", SCOUNT)

